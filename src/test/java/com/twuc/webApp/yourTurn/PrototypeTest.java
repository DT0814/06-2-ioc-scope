package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class PrototypeTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    //第二种简单的 scope 就是 prototype scope 了。请将类型 SimplePrototypeScopeClass 设置为 prototype scope。并尝试使用 getBean() 方法获得两个实例的引用。
    // 那么这两个引用指向的是同一个对象呢还是不同的对象呢？请写测试证明这一点。
    @Test
    void test_prototype() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);
        Assertions.assertNotSame(bean, bean2);
    }

    //我们刚才看到了两种 scope。我相信你已经能够明白，我们可以通过 getBean() 的方法获得指定类型的实例。但是问题是，这个实例是什么时候创建的呢？
    // 请书写若干测试来证明（1）对于 singleton 的对象，类型的实例是什么时候创建的？（2）对于 prototype scope 的对象，类型的实例是什么时候创建的？
    @Test
    void test_init() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass simplePrototypeScopeClass1 = context.getBean(SimplePrototypeScopeClass.class);
        ChildObject childObject = context.getBean(ChildObject.class);
        ChildObject childObject1 = context.getBean(ChildObject.class);

        MyLogger myLogger = context.getBean(MyLogger.class);
        Assertions.assertIterableEquals(Arrays.asList("chlidInit", "simplePrototype", "simplePrototype"), myLogger.getLines());
    }

    //Eager singleton initialization 有很多的好处。但是如果我无论如何也想在调用 getBean() 的时候再创建实例应该如何做呢？请书写测试来说明这种情况应该如何应对。
    @Test
    void lazy_init() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        Assertions.assertEquals(0, myLogger.getLines().size());
        SimpleSingletonInitTime initTime = context.getBean(SimpleSingletonInitTime.class);
        Assertions.assertIterableEquals(Arrays.asList("SimpleSingletonInitTime init"), myLogger.getLines());
    }

    //如果一个 prototype scope 的对象 PrototypeScopeDependsOnSingleton，依赖一个 singleton scope SingletonDependent 类型。
    // 那么请问如果多次调用 getBean(PrototypeScopeDependsOnSingleton.class) 的话（假设两次），
    // 那么请问 PrototypeScopeDependsOnSingleton 类型的实例创建了多少？而 SingletonDependent 类型的实例创建了多少？写测试证明你的猜想。
    @Test
    void test_singleton_init_count() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        PrototypeScopeDependsOnSingleton prototypeScopeDependsOnSingleton = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton prototypeScopeDependsOnSingleton2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        Assertions.assertIterableEquals(Arrays.asList("init SingletonDependent"), myLogger.getLines());

    }


    //如果一个 singleton scope 的对象，SingletonDependsOnPrototype 依赖一个 prototype scope 的对象 PrototypeDependent。
    // 请问，如果多次调用 getBean(SingletonDependsOnPrototype.class) 的话（假设两次），
    // 那么 SingletonDependsOnPrototype 创建了多少实例？而 PrototypeDependent 创建了多少实例？写测试证明你的猜想。

    @Test
    void test_prototype_init_count() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        SingletonDependsOnPrototype SingletonDependsOnPrototype = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype SingletonDependsOnPrototype2 = context.getBean(SingletonDependsOnPrototype.class);
        Assertions.assertIterableEquals(Arrays.asList("init SingletonDependent", "init PrototypeDependent"), myLogger.getLines());
    }
}
