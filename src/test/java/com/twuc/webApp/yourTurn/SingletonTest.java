package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SingletonTest {
    //如果我有一个类型 InterfaceOneImpl 实现了接口 InterfaceOne，那么 getBean(InterfaceOneImpl.class) 和 getBean(InterfaceOne.class)。
    // 会不会得到同一个实例呢？写测试证明这一点。
    //类似上述问题。对于类型的继承呢？对基类和派生类类型调用 getBean() 方法会得到同一个实例么？写测试证明这一点。
    //仍然类似上述问题。如果假设类 DerivedClass 继承了类 AbstractBaseClass。且 AbstractBaseClass 是一个抽象类。
    // 那么分别对 AbstractBaseClass.class 和 DerivedClass.class 调用 getBean() 会出现什么现象呢？写测试验证这一点。
    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void test_interface() {
        InterfaceOne bean = context.getBean(InterfaceOne.class);
        InterfaceOne bean1 = context.getBean(InterfaceOne.class);
        Assertions.assertSame(bean, bean1);
    }

    @Test
    void test_object() {
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        Assertions.assertSame(abstractBaseClass, derivedClass);
    }
}
