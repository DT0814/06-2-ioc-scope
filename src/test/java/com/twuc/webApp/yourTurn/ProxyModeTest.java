package com.twuc.webApp.yourTurn;

import com.twuc.webApp.MyLogger;
import com.twuc.webApp.SingletonDependsOnPrototypeProxy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProxyModeTest {
    AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }


    //现在有一个 singleton scope 的对象 SingletonDependsOnPrototypeProxy 依赖一个 PrototypeDependentWithProxy。
    // 如果我希望每一次调用 getBean(SingletonDependsOnPrototypeProxy.class)
    // 且使用该实例中的 PrototypeDependentWithProxy 类型的 field 的方法时都会创建一个新的 PrototypeDependentWithProxy 实例。
    // 该如何进行配置？并写测试证明自己的解决方案是正确的。

    @Test
    void test_proxy_get_bean() {
        MyLogger logger = context.getBean(MyLogger.class);
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        //Assertions.assertEquals(logger.getLines(),"");
        bean.getPrototypeDependentWithProxy().fun();
        bean.getPrototypeDependentWithProxy().fun();
        assertEquals(logger.getLines(), "");
    }

    //如果 singleton scope 对象 SingletonDependsOnPrototypeProxyBatchCall
    // 依赖 proxy mode 的 prototype scope 对象 PrototypeDependentWithProxy，
    // 并且singleton 中的一个方法连续调用了 prototype scope 类型的实例没有独立实现的方法（例如toString）两次，那么请问每一种对象实际创建了几个呢？写测试来证明你的想法。
    @Test
    void test_proxy_get_bean2() {
        MyLogger logger = context.getBean(MyLogger.class);
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy bean1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        //Assertions.assertEquals(logger.getLines(),"");
        bean.getPrototypeDependentWithProxy().toString();
        bean.getPrototypeDependentWithProxy().toString();
        assertEquals(logger.getLines(), "");
    }

    //现在有一个 singleton scope 的对象 SingletonDependsOnPrototypeProxy
    // 依赖一个 PrototypeDependentWithProxy。你能够写测试证明你使用的 PrototypeDependentWithProxy 是一个 Proxy 么？
    @Test
    void test_proxy_get_bean3() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertTrue(bean.getPrototypeDependentWithProxy().getClass().getSimpleName().contains("Proxy"));
    }
}
