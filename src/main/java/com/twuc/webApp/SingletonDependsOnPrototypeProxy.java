package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author tao.dong
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class SingletonDependsOnPrototypeProxy {
    public com.twuc.webApp.PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return PrototypeDependentWithProxy;
    }

    private PrototypeDependentWithProxy PrototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxy(com.twuc.webApp.PrototypeDependentWithProxy prototypeDependentWithProxy) {
        PrototypeDependentWithProxy = prototypeDependentWithProxy;
    }
}
