package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author tao.dong
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimplePrototypeScopeClass {

    private MyLogger logger;

    public SimplePrototypeScopeClass(MyLogger logger) {
        this.logger = logger;
        this.logger.log("simplePrototype");
    }
}
