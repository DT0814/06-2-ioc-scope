package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tao.dong
 */
@Component
public class MyLogger {
    private List<String> lines = new ArrayList<>();

    public void log(String log) {
        lines.add(log);
    }

    public List<String> getLines() {
        return lines;
    }
}
