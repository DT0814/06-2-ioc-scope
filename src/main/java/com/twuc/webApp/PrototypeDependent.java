package com.twuc.webApp;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author tao.dong
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeDependent {
    private MyLogger myLogger;

    public PrototypeDependent(MyLogger myLogger) {
        this.myLogger = myLogger;
        myLogger.log("init PrototypeDependent");
    }
}
