package com.twuc.webApp;

import org.springframework.stereotype.Component;

/**
 * @author tao.dong
 */
@Component
public class SingletonDependent {
    private MyLogger myLogger;

    public SingletonDependent(MyLogger myLogger) {
        this.myLogger = myLogger;
        myLogger.log("init SingletonDependent");
    }
}
